/**
 * Created by instancetype on 6/8/14.
 */
var net = require('net'),
    redis = require('redis');

var server = net.createServer(function(socket) {
    var subscriber,
        publisher;

    socket.on('connect', function() {
        subscriber = redis.createClient();
        subscriber.subscribe('main_chatroom');

        subscriber.on('message', function(channel, message) {
            socket.write('Channel ' + channel + ': ' + message);
        });

        publisher = redis.createClient();
    });

    socket.on('data', function(data) {
        publisher.publish('main_chatroom', data);
    });

    socket.on('end', function() {
        subscriber.unsubscribe('main_chatroom');
        subscriber.end();
        publisher.end();
    });
}).listen(3000);
